# Anforderungen

## Positionsdaten
 - Longitude
 - Latitude
 - Höhe
 - Genauigkeit - Signalstärke
 - Datum
 - GeräteID

## Batteriebertrieb
 - Ladbar
 - Ladungsdauer min 1 Tag
 - Ladungszustand
  
## LoRaWAN
 - 868 MHz The Things Network

## Formfaktor
 - Klein -> Halsband
 - Leicht
 - Flach
 - Spritzwasser geschützt

## GPS Modul
 - Schaltbare Versorgungsspannung
 - UART Interface
 - GPS/GNNS 

## Bewegungssensor
 - Interrupt auslösen, wenn eine neue Position gesendet werden soll
 - Dauer An Modus